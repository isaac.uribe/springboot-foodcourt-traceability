package com.example.springbootfoodcourttraceability.application.mapper;

import com.example.springbootfoodcourttraceability.application.dto.LogResponse;
import com.example.springbootfoodcourttraceability.domain.model.Log;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedSourcePolicy = ReportingPolicy.IGNORE,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface LogResponseMapper {
    LogResponse toLogResponse(Log log);
    List<LogResponse> toLogResponseList(List<Log> logs);
}

package com.example.springbootfoodcourttraceability.domain.model;

public enum Permission {
    EMPLOYEE_CREATE_LOG("employee:createlog"),
    CUSTOMER_GET_LOGS("customer:getlogs");


    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

    private final String permission;
}


package com.example.springbootfoodcourttraceability.application.handler;

import com.example.springbootfoodcourttraceability.application.dto.LogRequest;
import com.example.springbootfoodcourttraceability.application.dto.LogResponse;

import java.util.List;

public interface LogHandler {
    void createLog(LogRequest logRequest);
    List<LogResponse> getAllLogs(Long idCustomer, Long idOrder);
}

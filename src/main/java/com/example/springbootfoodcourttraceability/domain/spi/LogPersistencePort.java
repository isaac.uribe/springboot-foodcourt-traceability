package com.example.springbootfoodcourttraceability.domain.spi;

import com.example.springbootfoodcourttraceability.domain.model.Log;

import java.util.List;

public interface LogPersistencePort {
    void createLog(Log log);
    List<Log> getAllLogs(Long idCustomer, Long idOrder);
}

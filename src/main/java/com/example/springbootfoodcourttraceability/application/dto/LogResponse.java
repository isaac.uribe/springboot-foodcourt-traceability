package com.example.springbootfoodcourttraceability.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class LogResponse {
    private LocalDateTime dateTime;
    private String previousStatus;
    private String newStatus;
}

package com.example.springbootfoodcourttraceability.infrastructure.feightclient;

import com.example.springbootfoodcourttraceability.domain.model.UserAuth;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "foodcourtusers", url = "http://localhost:8080")
public interface UserFeightClient {
    @GetMapping("/auth/getByEmail/{email}")
    UserAuth getUserByEmail(@PathVariable(value = "email") String email);
}

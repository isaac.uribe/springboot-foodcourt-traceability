<br />
<div align="center">
<h3 align="center">Spring Boot FoodCourt Traceability</h3>
<p>
This is a backend project that is responsible for managing the traceability of the order status logs
</p>
</div>

### Build With
* ![Java](https://img.shields.io/badge/java-%23ED8B00.svg?style=for-the-badge&logo=java&logoColor=white)
* ![Spring](https://img.shields.io/badge/Spring-6DB33F?style=for-the-badge&logo=spring&logoColor=white)
* ![Gradle](https://img.shields.io/badge/Gradle-02303A.svg?style=for-the-badge&logo=Gradle&logoColor=white)
* ![MongoDB](https://img.shields.io/badge/MongoDB-%234ea94b.svg?style=for-the-badge&logo=mongodb&logoColor=white)

### Getting Started

To get a local copy up and running follow these steps.

### Prerequisites
* JDK 17
* Gradle
* MongoDB

### Instalation
1. Clone The Repo
2. Created a new databse in MongoDB called foodcourt
3. Update the database connection settings
 ```yml
   # src/main/resources/application.yml   
   spring:
    data:
     mongodb:
       host: localhost
       port: 27017
       database: foodcourt
   ```

## Usage
1. Open [http://localhost:8090/swagger-ui/index.html#/](http://localhost:8090/swagger-ui/index.html#/)
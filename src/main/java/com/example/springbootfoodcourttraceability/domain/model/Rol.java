package com.example.springbootfoodcourttraceability.domain.model;


public class Rol {
    private Long id;

    private Role  rol;
    private String description;

    public Rol() {
    }

    public Rol(Long id, Role rol, String description) {
        this.id = id;
        this.rol = rol;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Role getRol() {
        return rol;
    }

    public void setRol(Role rol) {
        this.rol = rol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

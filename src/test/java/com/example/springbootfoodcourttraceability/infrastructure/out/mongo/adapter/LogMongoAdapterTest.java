package com.example.springbootfoodcourttraceability.infrastructure.out.mongo.adapter;

import com.example.springbootfoodcourttraceability.domain.model.Log;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.entity.LogEntity;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.mapper.LogEntityMapper;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.repository.LogRepository;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class LogMongoAdapterTest {

    @InjectMocks
    LogMongoAdapter logMongoAdapter;
    @Mock
    LogRepository logRepository;
    @Mock
    LogEntityMapper logEntityMapper;

    @Test
    void createLog() {
        Log log = new Log();
        logMongoAdapter.createLog(log);
        Mockito.verify(logRepository, Mockito.times(1)).save(logEntityMapper.toLogEntity(log));
    }

    @Test
    void getAllLogs() {
        List<LogEntity> logEntityList = new ArrayList<>();
        logEntityList.add(new LogEntity());

        Mockito.when(logRepository.findByIdCustomerAndIdOrder(1L,1L)).thenReturn(logEntityList);
        List<Log> logs = logMongoAdapter.getAllLogs(1L,1L);
        assertEquals(logs, logEntityMapper.toLogList(logEntityList));
    }
}
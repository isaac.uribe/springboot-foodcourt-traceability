package com.example.springbootfoodcourttraceability.infrastructure.out.mongo.repository;

import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.entity.LogEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogRepository extends MongoRepository<LogEntity, String> {
    List<LogEntity> findByIdCustomerAndIdOrder(Long idCustomer, Long idOrder);
}

package com.example.springbootfoodcourttraceability.infrastructure.exceptionhandler;

import com.example.springbootfoodcourttraceability.infrastructure.exception.UserNotFound;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {
    private static final String MESSAGE = "Message";
    @ExceptionHandler(UserNotFound.class)
    public ResponseEntity<Map<String,String>> userNotFound(UserNotFound userNotFound){
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(Collections.singletonMap(MESSAGE, ExceptionResponse.USER_NOT_FOUND.getMessage()));
    }
}

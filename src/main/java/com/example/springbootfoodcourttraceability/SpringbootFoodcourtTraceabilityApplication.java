package com.example.springbootfoodcourttraceability;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class SpringbootFoodcourtTraceabilityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootFoodcourtTraceabilityApplication.class, args);
	}

}

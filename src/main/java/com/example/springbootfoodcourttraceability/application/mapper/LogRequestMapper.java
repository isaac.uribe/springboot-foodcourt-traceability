package com.example.springbootfoodcourttraceability.application.mapper;

import com.example.springbootfoodcourttraceability.application.dto.LogRequest;
import com.example.springbootfoodcourttraceability.domain.model.Log;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface LogRequestMapper {
    Log toLog(LogRequest logRequest);
}

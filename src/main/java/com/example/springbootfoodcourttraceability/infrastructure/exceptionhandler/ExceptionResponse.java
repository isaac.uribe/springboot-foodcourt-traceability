package com.example.springbootfoodcourttraceability.infrastructure.exceptionhandler;

public enum ExceptionResponse {
    USER_NOT_FOUND("usuario no encontrado");
    private String message;

    ExceptionResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}

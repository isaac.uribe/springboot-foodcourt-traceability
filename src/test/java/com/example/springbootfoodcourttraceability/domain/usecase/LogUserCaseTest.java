package com.example.springbootfoodcourttraceability.domain.usecase;

import com.example.springbootfoodcourttraceability.domain.model.Log;
import com.example.springbootfoodcourttraceability.domain.spi.LogPersistencePort;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
class LogUserCaseTest {
    @InjectMocks
    LogUserCase logUserCase;
    @Mock
    LogPersistencePort logPersistencePort;

    @Test
    void createLog() {
        Log log = new Log();
        logPersistencePort.createLog(log);
        Mockito.verify(logPersistencePort, Mockito.times(1)).createLog(log);
    }

    @Test
    void getAllLogs() {
        List<Log> logs = logUserCase.getAllLogs(1L, 2L);
        Mockito.verify(logPersistencePort, Mockito.times(1)).getAllLogs(1L,2L);
    }
}
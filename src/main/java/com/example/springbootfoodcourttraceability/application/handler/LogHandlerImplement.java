package com.example.springbootfoodcourttraceability.application.handler;

import com.example.springbootfoodcourttraceability.application.dto.LogRequest;
import com.example.springbootfoodcourttraceability.application.dto.LogResponse;
import com.example.springbootfoodcourttraceability.application.mapper.LogRequestMapper;
import com.example.springbootfoodcourttraceability.application.mapper.LogResponseMapper;
import com.example.springbootfoodcourttraceability.domain.api.LogServicePort;
import com.example.springbootfoodcourttraceability.domain.model.Log;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class LogHandlerImplement implements LogHandler{
    private final LogServicePort logServicePort;
    private final LogRequestMapper logRequestMapper;
    private final LogResponseMapper logResponseMapper;

    @Override
    public void createLog(LogRequest logRequest) {
        Log log = logRequestMapper.toLog(logRequest);
        logServicePort.createLog(log);
    }

    @Override
    public List<LogResponse> getAllLogs(Long idCustomer, Long idOrder) {
        List<Log> logList = logServicePort.getAllLogs(idCustomer, idOrder);
        return logResponseMapper.toLogResponseList(logList);
    }
}

package com.example.springbootfoodcourttraceability.infrastructure.exception;

public class UserNotFound extends RuntimeException{
    public UserNotFound() {
        super();
    }
}

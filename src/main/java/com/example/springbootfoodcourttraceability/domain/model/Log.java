package com.example.springbootfoodcourttraceability.domain.model;

import java.time.LocalDateTime;
public class Log {

    private String id;
    private Long idOrder;
    private Long idCustomer;
    private String emailCustomer;
    private LocalDateTime dateTime;
    private String previousStatus;
    private String newStatus;
    private Long idEmployee;
    private String emailEmployee;

    public Log() {
    }

    public Log(String id, Long idOrder, Long idCustomer, String emailCustomer, LocalDateTime dateTime,
               String previousStatus, String newStatus, Long idEmployee, String emailEmployee) {
        this.id = id;
        this.idOrder = idOrder;
        this.idCustomer = idCustomer;
        this.emailCustomer = emailCustomer;
        this.dateTime = dateTime;
        this.previousStatus = previousStatus;
        this.newStatus = newStatus;
        this.idEmployee = idEmployee;
        this.emailEmployee = emailEmployee;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(Long idOrder) {
        this.idOrder = idOrder;
    }

    public Long getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Long idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getEmailCustomer() {
        return emailCustomer;
    }

    public void setEmailCustomer(String emailCustomer) {
        this.emailCustomer = emailCustomer;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getPreviousStatus() {
        return previousStatus;
    }

    public void setPreviousStatus(String previousStatus) {
        this.previousStatus = previousStatus;
    }

    public String getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(String newStatus) {
        this.newStatus = newStatus;
    }

    public Long getIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(Long idEmployee) {
        this.idEmployee = idEmployee;
    }

    public String getEmailEmployee() {
        return emailEmployee;
    }

    public void setEmailEmployee(String emailEmployee) {
        this.emailEmployee = emailEmployee;
    }
}

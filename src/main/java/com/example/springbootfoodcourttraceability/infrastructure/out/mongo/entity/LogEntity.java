package com.example.springbootfoodcourttraceability.infrastructure.out.mongo.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
@Document(collection = "traceability")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class LogEntity {
    @Id
    private String id;
    private Long idOrder;
    private Long idCustomer;
    private String emailCustomer;
    private LocalDateTime dateTime;
    private String previousStatus;
    private String newStatus;
    private Long idEmployee;
    private String emailEmployee;
}

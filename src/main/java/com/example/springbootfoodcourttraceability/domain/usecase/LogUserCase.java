package com.example.springbootfoodcourttraceability.domain.usecase;

import com.example.springbootfoodcourttraceability.domain.api.LogServicePort;
import com.example.springbootfoodcourttraceability.domain.model.Log;
import com.example.springbootfoodcourttraceability.domain.spi.LogPersistencePort;

import java.util.List;

public class LogUserCase implements LogServicePort {
    private final LogPersistencePort logPersistencePort;

    public LogUserCase(LogPersistencePort logPersistencePort) {
        this.logPersistencePort = logPersistencePort;
    }

    @Override
    public void createLog(Log log) {
        logPersistencePort.createLog(log);
    }

    @Override
    public List<Log> getAllLogs(Long idCustomer, Long idOrder) {
        return logPersistencePort.getAllLogs(idCustomer,idOrder);
    }
}

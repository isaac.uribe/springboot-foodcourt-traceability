package com.example.springbootfoodcourttraceability.infrastructure.input.rest;

import com.example.springbootfoodcourttraceability.application.dto.LogRequest;
import com.example.springbootfoodcourttraceability.application.dto.LogResponse;
import com.example.springbootfoodcourttraceability.application.handler.LogHandler;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/log")
@RequiredArgsConstructor
public class LogRestController {
    private final LogHandler logHandler;
    @Operation(summary = "Create a log")
    @ApiResponse(responseCode = "201", description = "Log created successfully")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @ApiResponse(responseCode = "400", description = "Bad Request")
    @PostMapping
    public ResponseEntity<Void> saveLog(@RequestBody LogRequest logRequest) {
        logHandler.createLog(logRequest);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
    @Operation(summary = "Get all logs from the database by IdCustomer and IdOrder")
    @ApiResponse(responseCode = "200", description = "All logs found")
    @ApiResponse(responseCode = "403", description = "Unauthorized / Invalid Token")
    @GetMapping
    public ResponseEntity<List<LogResponse>> getAllLogs(@RequestParam Long idCustomer, @RequestParam Long idOrder){
        return ResponseEntity.ok(logHandler.getAllLogs(idCustomer, idOrder));
    }
}

package com.example.springbootfoodcourttraceability.infrastructure.out.mongo.adapter;

import com.example.springbootfoodcourttraceability.domain.model.Log;
import com.example.springbootfoodcourttraceability.domain.spi.LogPersistencePort;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.entity.LogEntity;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.mapper.LogEntityMapper;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.repository.LogRepository;
import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@RequiredArgsConstructor
public class LogMongoAdapter implements LogPersistencePort {

    private final LogRepository logRepository;
    private final LogEntityMapper logEntityMapper;
    @Override
    public void createLog(Log log) {
        log.setDateTime(LocalDateTime.now());
        LogEntity logEntity = logEntityMapper.toLogEntity(log);
        logRepository.save(logEntity);
    }
    @Override
    public List<Log> getAllLogs(Long idCustomer, Long idOrder) {
        List<LogEntity> logEntityList = logRepository.findByIdCustomerAndIdOrder(idCustomer, idOrder);
        return logEntityMapper.toLogList(logEntityList);
    }
}

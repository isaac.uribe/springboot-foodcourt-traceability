package com.example.springbootfoodcourttraceability.infrastructure.configuration;

import com.example.springbootfoodcourttraceability.domain.api.LogServicePort;
import com.example.springbootfoodcourttraceability.domain.spi.LogPersistencePort;
import com.example.springbootfoodcourttraceability.domain.usecase.LogUserCase;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.adapter.LogMongoAdapter;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.mapper.LogEntityMapper;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.repository.LogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {
    private final LogRepository logRepository;
    private final LogEntityMapper logEntityMapper;

    @Bean
    public LogPersistencePort logPersistencePort(){
        return new LogMongoAdapter(logRepository, logEntityMapper);
    }
    @Bean
    public LogServicePort logServicePort(){
        return new LogUserCase(logPersistencePort());
    }
}

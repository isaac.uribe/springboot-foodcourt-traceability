package com.example.springbootfoodcourttraceability.application.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Getter
@Setter
public class LogRequest {
    private Long idOrder;
    private Long idCustomer;
    private String emailCustomer;
    private String previousStatus;
    private String newStatus;
    private Long idEmployee;
    private String emailEmployee;
}

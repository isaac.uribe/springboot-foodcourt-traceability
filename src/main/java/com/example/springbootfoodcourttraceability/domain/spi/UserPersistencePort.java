package com.example.springbootfoodcourttraceability.domain.spi;

import com.example.springbootfoodcourttraceability.domain.model.UserAuth;

public interface UserPersistencePort {
    UserAuth getUserByEmail(String email);
}

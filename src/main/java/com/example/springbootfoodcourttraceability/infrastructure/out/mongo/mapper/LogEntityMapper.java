package com.example.springbootfoodcourttraceability.infrastructure.out.mongo.mapper;

import com.example.springbootfoodcourttraceability.domain.model.Log;
import com.example.springbootfoodcourttraceability.infrastructure.out.mongo.entity.LogEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(
        componentModel = "spring",
        unmappedTargetPolicy = ReportingPolicy.IGNORE,
        unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface LogEntityMapper {
    LogEntity toLogEntity(Log log);
    Log toLog(LogEntity logEntity);
    List<Log> toLogList(List<LogEntity> logEntityList);
}

package com.example.springbootfoodcourttraceability.domain.api;

import com.example.springbootfoodcourttraceability.domain.model.Log;

import java.util.List;

public interface LogServicePort {
    void createLog(Log log);
    List<Log> getAllLogs(Long idCustomer, Long idOrder);
}

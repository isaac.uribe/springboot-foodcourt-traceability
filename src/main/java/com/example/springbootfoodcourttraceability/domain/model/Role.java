package com.example.springbootfoodcourttraceability.domain.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public enum Role {

    USER(Collections.emptySet()),

    EMPLOYEE(
            Set.of(
                   Permission.EMPLOYEE_CREATE_LOG
            )
    ),
    CUSTOMER(
            Set.of(
                    Permission.CUSTOMER_GET_LOGS
            )
    );
    Role(Set<Permission> permissions) {
        this.permissions = permissions;
    }

    public Set<Permission> getPermissions() {
        return permissions;
    }

    private Set<Permission> permissions;

    public List<SimpleGrantedAuthority> getAuthorities(){
        var authorities = getPermissions()
                .stream()
                .map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
                .collect(Collectors.toList());
        authorities.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
        return authorities;
    }
}
